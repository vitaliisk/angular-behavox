import { Injectable } from '@angular/core';
import * as data from './email.json';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  public jsonData: any = (data as any).default;

  constructor() {}

}
